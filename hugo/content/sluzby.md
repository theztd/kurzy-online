---
title: "Služby"
description: ""
images: []
draft: false
menu: disabled_main
weight: 4
---

### Individuální poradenství:
  * Efektivita týmu i jednotlivců
  * Práce v distribuovaných týmech
  * Řízení distribuovaného týmu

### Správa a nasazení online nástrojů
  * GSuite
  * Poradenství s přechodem do AWS
  * Automatizace IT
  * Správa serverů Linux

### Pomoc s vlastní výukou na internetu
  * Výběr nástrojů
  * Prezentační dovednosti
  * Pomoc se zpracováním tématu/kurzu

### Anglický jazyk
  * Pomoc s mluveným projevem
  * Bussiness a Finanční Angličtina
  * Oprava a překlady textů (web, korespondence, prezentace)
  * Zlepšování psaného projevu metodou opravy Vašich textů
 

