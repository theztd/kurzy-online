---
title: "IT dovednosti"
description: "Online, ale i prezenční kurzy pro budoucí IT odborníky"
draft: false
menu: main
weight: 2
keywords: kurzy online, G-Suite, Nomad, Ansible, Gitlab, Linux
---

IaaC pro garážovky (4-16h)
=========================

![IaaC pro garážovky](/images/infra_schema.png#floatleft-big)

V tomto kurzu si sami postavíte škálovatelné, moderní řešení, které stačí udržovat pár hodin měsíčně a porovnáme si ho prakticky s dalšími přístupy (plný outsourcing, AWS/GCP/Azure cloud, vlastními silami od začátku). Na konci kurzu bude mít každý účastník funkční **infrastrukturu pod $100/měsíčně**, kterou po tomto kurzu bude schopen rozvíjet a spravovat, ale i schopnost rozhodnout, zda touto cestou skutečně chcete jit.


Účastník kurzu musí vědět co je **Linux** a neměl by bát **GIT**u, už by měl tušit k čemu je **Ansible** a vědět co je yaml. V ideálním případě by měl mít základní znalosti programování. 

Kurz nabízím ve 2 podobách:
- **superintenzivní konzultace (4-6h)** - pro pokročilejší uživatele, nebo programátory
- **podrobný a praktický workshop (4x4h)**  - pro juniorní lidi

Max počet účastníků je 6

Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------


Ansible (4-8h)
==============

![IaaC pro garážovky](/images/infra_schema.png#floatleft-big)

V tomto kurzu si sami postavíte škálovatelné, moderní řešení, které stačí udržovat pár hodin měsíčně a porovnáme si ho prakticky s dalšími přístupy (plný outsourcing, AWS/GCP/Azure cloud, vlastními silami od začátku). Na konci kurzu bude mít každý účastník funkční **infrastrukturu pod $100/měsíčně**, kterou po tomto kurzu bude schopen rozvíjet a spravovat, ale i schopnost rozhodnout, zda touto cestou skutečně chcete jit.

Max počet účastníků je 6

Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------


GSuite/Gmail chytře  (3-5h)
===========================

![Gmail chytře](/images/gmail_core_apps.png#floatright-big)

Gmail může mít každý zdarma a mnoho z Vás už ho dokonce má. Gmail není jen email, je to velmi promyšlená sada pomůcek pro každý den Vašeho osobního i pracovního života. Lze s ním chodit nakupovat, organizovat dovolenou, pracovat na domácích úkolech, spravovat recepty po babičce nebo domácí rozpočet. S pomocí pouhého GSuite/gmailu můžete řídit malou i větší firmu řemeslníků (dnes má každý chytrý telefon), penzion, ale uděláte s ním velkou parádu i při digitalizaci Vaší školy, úřadu, nebo jen týmu kolegů.

Nabízím Vám úvodní kurz, kde nebudeme bezcílně proklikávat aplikace, ale kde si libovolnou z  výše uvedených situací přímo vyzkoušíme.


Max počet účastníků je 10

Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------



Zde uvedené kurzy stojí od 2000,- za 1h. S velikostí skupiny se náklady rozkládají.