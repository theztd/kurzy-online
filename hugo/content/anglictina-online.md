---
title: "Angličtina online"
description: "INDIVIDUÁLNÍ ONLINE LEKCE ANGLIČTINY, které Vás rozmluví - ODKUDKOLIV"
draft: false
menu: main
weight: 2
aliases:
- /anglictina
- /prakticka-anglictina
keywords: kurzy angličtiny, angličtina online, individualní angličtina, praktická angličtina
---


## Individuální online lekce angličtiny, které Vás rozmluví (odkudkoliv)


### Co mi tento online kurz angličtiny přinese?

  * Budu schopný/schopná se v angličtině domluvit bez ohledu na moji aktuální úroveň jazyka
  * Postupně zapojím AJ do svého každodenního života
  * Zlepším svůj mluvený projev, používání časů, a budu se lépe orientovat v gramatice

a navíc se zbavím obav či strachu z komunikace v AJ :-) 

{{< youtube id="JcDICHGiGmI" >}}


### Proč bych si měl vybrat právě tento kurz? V čem je jedinečný?

  * Lekce stavíme tak, aby se studenti nejen zlepšovali, ale taky zažívali radost z komunikace 
  * Vytváříme studijní strategii i materiály na míru každému studentovi dle jeho potřeb
  * Využíváme technologie jako google dokumenty, whatsapp, skype, díky kterým máte studijní materiály vždy po ruce a navíc máte neustálý přehled o svém pokroku
  * Zaměřujeme se na každodenní úspěchy a zlepšení, ne na chyby
  * Poskytujeme překladatelské/poradenské služby i nad rámec kurzu  (např. pomoc při překladech životopisů)
  * V případě zájmu se studenty udržujeme komunikaci i mimo lekci (každodenní zlepšování)


Kurzy AJ na míru pro vás tvoří a vede lektorka [Markéta Kreuzingerová](/lektori/#marketa-kreuzingerova)


